# app.vue.symfony

## Project setup
```
composer install
npm install
```

### Compiles and hot-reloads for development
```
you can run it on docker or other app ( ex. laragon )
npm run dev or npm run watch (recommend) 
```

### Compiles and minifies for production
```
npm run build
```

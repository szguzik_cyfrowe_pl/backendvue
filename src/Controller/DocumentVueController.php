<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DocumentVueController extends AbstractController
{
    /**
     * @Route("/", name="index")
     * @IsGranted("ROLE_USER")
     */
    public function index(): Response
    {
        $content = 'To jest już Vue a ten tekst jest przekazany z backendu';

        return $this->render('document_vue/index.html.twig', [
            'content' => $content,
        ]);
    }


    /**
     * @Route("/companies", name="companies")
     */
    public function companies(): Response
    {
        return $this->render('document_vue/copmanies.html.twig');
    }

    /**
     * @Route("/modal", name="modal")
     */
    public function modalPage(): Response
    {
        return $this->render('document_vue/modal.html.twig');
    }
}

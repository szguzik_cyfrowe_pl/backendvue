<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class ApiController extends AbstractController
{

    /**
     * @Route("/navigation", name="navigation")
     */
    public function navigation()
    {
        $nav = [
            [
                'name' => 'Strona domowa',
                'link' => './',
            ],
            [
                'name' => 'Firmy',
                'link' => './companies',
            ],
            [
                'name' => 'Modal',
                'link' => './modal',
            ],
        ];


        return new JsonResponse($nav, '200', ['content-type' => 'application/json; charset=utf-8']);
    }

    /**
     * @Route("/companies", name="companies")
     */
    public function companies(): JsonResponse
    {
        $companies = [
            [
                'name' => 'Cyfrowe',
                'country_short' => 'PL',
                'address' => 'Cyfrowe.pl ul. Łostowicka 25A 80-121 Gdańsk',
                'nip' => '3939049386',
                'IBAN' => '41109022553419235126635900',
            ],

            [
                'name' => 'Lornetki',
                'country_short' => 'PL',
                'address' => 'Cyfrowe.pl ul. Łostowicka 25A 80-121 Gdańsk',
                'nip' => '3965891386',
                'IBAN' => '41109045632419235126635900',
            ],
        ];

        return new JsonResponse($companies, '200', ['content-type' => 'application/json; charset=utf-8']);
    }

    /**
     * @Route("/users", name="users")
     */
    public function usersFaker()
    {
        $usersFaker = [
            [
                'id' => 1,
                'first_name' => 'Jesse',
                'last_name' => 'Simmons',
                'date' => '2016-10-15',
                'gender' => 'Male',
            ],
            [
                'id' => 2,
                'first_name' => 'John',
                'last_name' => 'Jacobs',
                'date' => '2016-12-15',
                'gender' => 'Male',
            ],
            [
                'id' => 3,
                'first_name' => 'Tina',
                'last_name' => 'Gilbert',
                'date' => '2016-04-26',
                'gender' => 'Female',
            ],
            [
                'id' => 4,
                'first_name' => 'Clarence',
                'last_name' => 'Flores',
                'date' => '2016-04-10',
                'gender' => 'Male',
            ],
            [
                'id' => 5,
                'first_name' => 'Anne',
                'last_name' => 'Lee',
                'date' => '2016-12-06',
                'gender' => 'Female',
            ],
        ];

        return new JsonResponse($usersFaker, '200', ['content-type' => 'application/json; charset=utf-8']);
    }
}

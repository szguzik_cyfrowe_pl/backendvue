import Vue from 'vue';
import './bootstrap';
import 'buefy/dist/buefy.css'

import Buefy from 'buefy'

Vue.use(Buefy)
import Homepage from "./pages/Homepage";
import Login from "./pages/Login";
import Companies from "./pages/Companies";
import Modalpage from "./pages/Modalpage";
import Navigationbar from "./theme/cyfrowe/components/Navigationbar";
import Container from "./theme/core/components/Container";

import axios from 'axios'

Vue.prototype.$axios = axios

new Vue({
    // render: h => h(Homepage)
    components: {
        Homepage,
        Navigationbar,
        Login,
        Companies,
        Container,
        Modalpage
    }
}).$mount('#app')
